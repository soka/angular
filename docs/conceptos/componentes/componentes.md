[TOC]

- ["Crear un componente de forma manual"](./crear-componente-forma-manual/crear-componente-forma-manual.md).
- ["Crear un componente con Angular CLI"](./crear-componente-con-cli/crear-componente-con-cli.md).
- ["Componentes en línea"](./componentes-en-linea/componentes-en-linea.md).
- ["Comunicación entre componentes"](./comunicacion-entre-componentes/comunicacion-entre-componentes.md).
