[TOC]

# Generar un nuevo componente con Angular CLI

```ts
ng generate component persona
```

O de forma abreviada `ng g c persona`.

De forma automática crea una nueva carpeta con los ficheros necesarios para el nuevo componente y actualiza "app.module.ts":

```ps
CREATE src/app/persona/persona.component.html (26 bytes)
CREATE src/app/persona/persona.component.spec.ts (635 bytes)
CREATE src/app/persona/persona.component.ts (273 bytes)
CREATE src/app/persona/persona.component.css (0 bytes)
UPDATE src/app/app.module.ts (490 bytes)
```

```ts
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { PersonasComponent } from "./personas/personas.component";
import { PersonaComponent } from "./persona/persona.component";

@NgModule({
  declarations: [AppComponent, PersonasComponent, PersonaComponent],
  imports: [BrowserModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
```

Podemos eliminar "persona.component.spec.ts" que se usa para pruebas o _testing_ ya que no lo usaremos por el momento.

Ahora editamos "persona.component.ts", podemos eliminar la implementación del interfaz `OnInit` de la declaración de la clase `PersonaComponent` ya que tiene ver con el ciclo de vida de la aplicación cuando arranca e invoca la función `ngOnInit`.

```ts
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-persona",
  templateUrl: "./persona.component.html",
  styleUrls: ["./persona.component.css"]
})
//export class PersonaComponent implements OnInit {
export class PersonaComponent {
  constructor() {}

  /** ngOnInit() {
  } */
}
```

Ahora añadimos el nuevo componente en "personas.component.html"

```ts
<h2>Listado de personas</h2>
<app-persona></app-persona>
```

![](img/01.PNG)

Podemos agregar más elementos o repetir el componente:

```ts
<h2>Listado de personas</h2>
<app-persona></app-persona>
<app-persona></app-persona>
```

# Código fuente del ejemplo

- angular / src / conceptos / componentes / [new-component-cli](https://gitlab.com/soka/angular/tree/master/src/conceptos/componentes/new-component-cli).

# Enlaces internos

- ["Crear un componente de forma manual"](http://127.0.0.1:8200/conceptos/componentes/crear-componente-forma-manual/crear-componente-forma-manual/).

# Enlaces externos
