[TOC]

Un pequeño ejercicio para seguir practicando con la interpolación de cadenas del modelo a la vista.

He creado un nuevo componente llamado "persona":

```ts
ng g c persona
```

He borrado el contenido HTML de la vista principal [src/app/app.component.html] y he incrustado el nuevo componente.

```html
<app-persona></app-persona>
```

# Modelo

[src/app/persona/persona.component.ts]

```ts
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-persona",
  templateUrl: "./persona.component.html",
  styleUrls: ["./persona.component.css"]
})
export class PersonaComponent implements OnInit {
  nombrePersona: string = "Iker";
  apellidoPersona: string = "Landajuela";
  edadPersona: number = 40;

  constructor() {}

  ngOnInit() {}
}
```

# Vista

[src/app/persona/persona.component.html]

```ts
<h2>Listado de personas</h2>
<p>
  Nombre: {{ nombrePersona }}, Apellido: {{ apellidoPersona }}, Edad:
  {{ edadPersona }}
</p>
```

Dentro de la expresión de interpolación podemos realizar operaciones, por ejemplo:

```ts
<p>Edad+1: {{ edadPersona + 1 }}</p>
```

# Atributos privados

Por defecto se infiere que los atributos de la clase son públicos, **cuando un atributo es privado no se puede acceder desde la vista**, en ese caso debemos definir un método para acceder.

```ts
  private alturaPersona: number = 1.7;

  getAlturaPersona(): number {
    return this.alturaPersona;
  }
```

```ts
<p>Altura: {{ getAlturaPersona(); }}</p>
```

# Bucle ngFor con interpolación

Componente:

```ts
  miArray: string[] = ["Uno", "Dos", "Tres"];
```

```ts
<ul *ngFor="let item of miArray">
  <li>{{ item }}</li>
</ul>
```

# Código fuente del ejemplo

- GitLab: angular / src / conceptos / data-binding / [interpolation](https://gitlab.com/soka/angular/tree/master/src/conceptos/data-binding/interpolation).

StackBlitz:

<iframe width="100%" height="600px" src="https://stackblitz.com/edit/string-interpolation-personas?embed=1&file=src/app/app.component.html"></iframe>
```
