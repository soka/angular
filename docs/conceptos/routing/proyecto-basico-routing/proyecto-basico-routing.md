[TOC]

# Sistema de enrutado en Angular

En un sitio Web normalmente los enlaces de navegación apuntan a diferentes rutas, cuando pinchamos sobre ellos el servidor Web recibe la petición, carga la nueva página y entrega la página solicitada en la nueva ruta, por ejemplo este artículo pertenece al sitio raíz [https://soka.gitlab.io/angular/](https://soka.gitlab.io/angular/), cuando pinchamos sobre un enlace en el menú lateral como por ejemplo [https://soka.gitlab.io/angular/intro/getting-started/getting-started/](https://soka.gitlab.io/angular/intro/getting-started/getting-started/), el servidor carga la página en una nueva ruta en "/angular/intro/getting-started/getting-started/".

Las Web construidas con **Angular** siguen el concepto SPA (Single Page Application), sólo tenemos una página principal "index.html", todas las vistas asociadas a cada componente se cargan sobre este index.html. Para facilitar la navegación entonces usamos el sistema de enrutado con rutas "virtuales".

# Creación de una app con enrutado habilitado

La opción de enrutado no está habilitada por defecto cuando creamos un nuevo proyecto, hay que indicar explicitamente que queremos usarla en las opciones de creación del proyecto.

```ps
ng new routing-basics-nav-menu
```

Uan vez creado el proyecto en [src/app/app.module.ts] **Angular** importa la clase "AppRoutingModule" del componente donde se configuran las rutas:

```ts
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module"; // <---- Route
import { AppComponent } from "./app.component";

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule // <---- Route
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
```

# Definiendo el menú de navegación

En [src/app/app.component.html] definimos la vista con el menú de navegación con el atributo [`routerLink`](https://angular.io/api/router/RouterLink) definimos la ruta de cada opción, los enlaces comienzan por "/" para referirse al raíz.

```ts
<nav>
  <a href="#" routerLink="/">Home</a> |
  <a href="#" routerLink="/contacto">Contacto</a> |
</nav>

<router-outlet></router-outlet>
```

Más abajo la directiva `<router-outlet>` inyecta un componente u otro dependiendo de la ruta elegida.

# Crear componentes adicionales para la navegación

Voy a crear el componente "contacto" al que hace referencia el menú.

```ps
ng g c contacto
```

He creado un nuevo componente [src/app/contacto/contacto.component.ts]. Usando el mismo sistema he creado un componente "home" con `ng g c home`.

# Crear la lista de rutas disponibles

En la cabecera de [src/app/app-routing.module.ts] se importan las clases necesarias:

```ts
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { ContactoComponent } from "./contacto/contacto.component"; // <---
import { HomeComponent } from "./home/home.component"; // <---
```

En el cuerpo se declara `routes` que es un array de objetos de tipo `Routes`, inicialmente vacío añado al array los componentes que corresponden a cada ruta en el menú.

```ts
const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "contacto", component: ContactoComponent }
];
```

# Ejecución

Con estos sencillos pasos ya tenemos en marcha nuestro menú de navegación entre componentes:

![](img/01.PNG)

# Dándole un poco de estilo con Bulma

![](img/02.PNG)

[Bulma](https://bulma.io/) es un _framework_ CSS responsivo y modular para dar estilo a nuestras páginas. Procedemos con la instalación en el raíz del proyecto:

```ps
npm install bulma
```

Una vez instalado modifico "angular.json":

```json
            "styles": [
              "node_modules/bulma/css/bulma.min.css",
              "src/styles.css"
            ],
```

Debemos parar y arrancar la app para que los cambios tengan efecto (`ng serve -o`).

Uso el [componente menú](https://bulma.io/documentation/components/menu/) de Bulma para decorar mi menú:

```ts
<aside class="menu">
  <p class="menu-label">
    General
  </p>
  <ul class="menu-list">
    <li><a href="#" routerLink="/">Home</a></li>
    <li><a href="#" routerLink="/contacto">Contacto</a></li>
  </ul>
</aside>

<router-outlet></router-outlet>
```

Ahora el menú tiene un aspecto más elegante:

![](img/03.PNG)

# Código fuente del proyecto

- angular / src / conceptos / routing / [routing-basics-nav-menu](https://gitlab.com/soka/angular/tree/master/src/conceptos/routing/routing-basics-nav-menu).

# Enlaces externos

- ["Router CLASS"](https://angular.io/api/router/Router): Especificación clase.
- ["Routing & Navigation - Angular"](https://angular.io/guide/router).
- ["Routing - Angular"](): Tutorial Tout of heroes.
- ["Introducción al sistema de Routing en Angular - Desarrolloweb"](https://desarrolloweb.com/articulos/introduccion-sistema-routing-angular.html).
- ["Tutorial paso a paso de Angular 7 #3: Routing - El blog de respag"](http://respagblog.azurewebsites.net/tutorial-paso-a-paso-de-angular-7-3-routing/).
