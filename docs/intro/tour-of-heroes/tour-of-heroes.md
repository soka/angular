[TOC]

![](img/my-app-01.PNG)

# Tutorial: Tour of Heroes

El tutorial oficial de la Web de **Angular** [_Tour of Heroes_](https://angular.io/tutorial#tutorial-tour-of-heroes) repasa los conceptos básicos del funcionamiento de **Angular**.

La aplicación tiene como fin gestionar una serie de "Heroes" con las siguientes funcionalidades: Mostrar una lista de héroes, editar los datos de un héroe, etc,.

Al finalizar este tutorial deberíamos saber: Usar las directivas de **Angular** para esconder elementos o mostrar una lista de datos de héroes, usar componentes Web de **Angular** para mostrar los detalles de un héroe, _data binding_ unidireccional para datos de solo lectura, añadir campos de edición modificables para poner en practica el _data binding_ bidireccional, formatear datos con tuberías (_pipes_)...

# Diseño y funcionamiento de la aplicación

Esta es una animación de como debe quedar nuestra aplicación:

![](img/toh-anim.gif)

## Dashboard

![](img/tour-01.png)

Los dos elementos de arriba son enlaces para ver el _dashboard_ de la imagen o una lista de heroes.

## Hero Details

![](img/tour-02.png)

## Lista de heroes

![](img/tour-03.png)

# Crear entorno de trabajo inicial

```JS
ng new angular-tour-of-heroes
cd angular-tour-of-heroes
ng serve --open
```

# Cambiando el título

Editamos el fichero con la clase componente "app.component.ts" y modificamos el valor de la propiedad `title`:

```JS
title = 'Tour of Heroes';
```

En la plantilla del componente "app.component.html" borramos todo el contenido y lo sustituimos por:

```JS
<h1>{{title}}</h1>
```

# Estilos

Para modificar los estilos CSS globales que afectan a toda nuestra aplicación **Angular** ha generado para el proyecto el fichero "src/styles.css" pero su contenido está vacío, añadimos las siguientes líneas:

```CSS
/* Application-wide Styles */
h1 {
  color: #369;
  font-family: Arial, Helvetica, sans-serif;
  font-size: 250%;
}
h2, h3 {
  color: #444;
  font-family: Arial, Helvetica, sans-serif;
  font-weight: lighter;
}
body {
  margin: 2em;
}
body, input[type="text"], button {
  color: #888;
  font-family: Cambria, Georgia;
}
/* everywhere else */
* {
  font-family: Arial, Helvetica, sans-serif;
}
```

¿Ahora el título se ve más elegante no?

![](img/tour-04.png)

# Nuevo componente editor de heroes

Vamos a crear un componente para trabajar con un héroe concreto.

```JS
ng generate component heroes
```

Hemos creado una ruta para un nuevo componente "src/app/heroes/" y sus ficheros asociados.

![](img/tour-05.png)

Inicialmente el nuevo componente "app/heroes/heroes.component.ts" tendrá el siguiente contenido:

```JS
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
```

[@Component](https://angular.io/api/core/Component) define los metadatos del componente con tres propiedades:

- `selector`: _ the component's CSS element selector_.
- `templateUrl`: Localización de la plantilla HTML del componente.
- `styleUrls`: Localización del CSS del componente.

`ngOnInit` es un _hook_ del ciclo de vida del componente, **Angular** invoca la función después de crear el componente, es el sitio adecuado para introducir cualquier código o lógica de inicio.

La palabra clave `export` permite que pueda ser usada en cualquier parte del proyecto.

# Añadiendo una propiedad al héroe

Añadimos un atributo a la clase:

```JS
hero = "Windstorm";
```

Borramos el contenido de la plantilla y añadimos:

```HTML
{{hero}}
```

# Visualizar el nuevo componente HeroesComponent

Añadimos a "src/app/app.component.html":

```HTML
<h1>{{title}}</h1>
<app-heroes></app-heroes>
```

![](img/tour-06.png)

# Nueva clase "Hero"

Creamos un fichero "src/app/hero.ts":

```JS
export class Hero {
  id: number;
  name: string;
}
```

Importamos la nueva clase en "src/app/heroes/heroes.component.ts":

```JS
import { Component, OnInit } from "@angular/core";
import { Hero } from "../hero";

@Component({
  selector: "app-heroes",
  templateUrl: "./heroes.component.html",
  styleUrls: ["./heroes.component.css"]
})
export class HeroesComponent implements OnInit {
  hero: Hero = {
    id: 1,
    name: "Windstorm"
  };

  constructor() {}

  ngOnInit() {}
}
```

La página se visualiza mal porque hemos cambiado la definición de una cadena inicial por un objeto:

![](img/tour-07.png)

Modificamos "heroes.component.html":

```JS
<h2>{{hero.name}} Details</h2>
<div><span>id: </span>{{hero.id}}</div>
<div><span>name: </span>{{hero.name}}</div>
```

![](img/tour-08.png)

# Enlaces externos

- angular.io ["Tutorial: Tour of Heroes"](https://angular.io/tutorial#tutorial-tour-of-heroes).
- [Ejemplo](https://angular.io/generated/live-examples/toh-pt6/stackblitz.html) en vivo en [StackBlitz](https://stackblitz.com/) (editor de código en línea).
