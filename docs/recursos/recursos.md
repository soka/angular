[TOC]

# Depurar con Chrome Dev Tools

https://juristr.com/blog/2016/02/debugging-angular2-console/

# Plugins para Visual Studio Code

- [Angular v7 Snippets](https://marketplace.visualstudio.com/items?itemName=johnpapa.Angular2).
- [Angular Language Service](https://marketplace.visualstudio.com/items?itemName=Angular.ng-template).
- [Angular 2 TypeScript Emmet](https://marketplace.visualstudio.com/items?itemName=jakethashi.vscode-angular2-emmet).
